'use strict';
console.log("Programming Fundamentals for Beginners");

/*Գրել ցիկլ, որը կարտածի զանգվածի էլեմենտները։*/


/*Հաշվե թվային զանգվածի էլեմենտների գումարը։*/


/*Հաշվել թվային զանգվածի էլեմենտների արտադրյալը։*/


/*Գտնել թվային զանգցածի էլեմենտներից փոքրագույնը։*/


/*Գտնել թվային զանգվածի էլեմենտներից մեծագույնը։*/

/*
function Max(a, b) {
	if (a > b) {
		return a;
	}
	else {
		return b;
	}
}
*/
//returns max of two numbers
var Max = function(a, b) {
	/*
	if (a > b) {
		return a;
	}
	else {
		return b;
	}
	*/
	var max = a;
	
	if(b > a) max = b;

	return max;
}

//returns max of array
var MaxArray = function(array) {
	var max = array[0];
	var i = 1;
	while(i < array.length) {
		/*
		if(array[i] > max) {
			max = array[i];
		}
		*/
		max = Max(max, array[i]);
		i += 1;
	}

	return max;
}

var Swap = function(a, b) {
	var c = a;
	a = b;
	b = c;
}

var Test = function(a) {
	console.log("before a = " + a);
	a = 5;
	console.log("after a = " + a);
}

var x = 5;
var y = 7;
var max1 = Max(x, y);

var a = 10, b = 11;
var max2 = Max(a, b);

var max3 = Max(3, 6);

var t = [1, 13, 35, 2, 3];
var max4 = MaxArray(t);


console.log(max1);
console.log(Max(8, 9));

console.log(max4);
console.log(MaxArray([2, 3, 5, 7, 11, 13, 17, 19]));

console.log(x + ", " + y);
console.log(Swap(x, y));
console.log(x + ", " + y);

var p = 1;
console.log("before function call p = " + p);
Test(p);
console.log("after function call p = " + p);

//Function Examples
//----------------------
//Function Declaration
var PrintHelloWorld = function() {
	console.log("Hello World");
}

//Funation Call
PrintHelloWorld();

var F1 = function(name) {
	console.log("Hello " + name);
}

F1("Bob");
F1("Alice");


var F2 = function() {
	return "Hi";
}

var message = F2();
//var message = "Hi";
console.log(message);
console.log(F2());

var F3 = function(x) {
	return 2 * x;
}

console.log(F3(5));
console.log(F3(7));
console.log(F3(9));