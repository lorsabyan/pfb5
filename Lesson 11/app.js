'use strict';
console.log("Programming Fundamentals for Beginners");

var factorial = function(n) {
	if(n == 1) return 1;
	return n * factorial(n - 1);
}

/*
for(var i = 1; i <= 170; i++) {
	console.log(i + "! = " + factorial(i));
}
*/

/*
var fibonacci = function(n) {
	if(n == 1) return 0;
	if(n == 2) return 1;
		
	return fibonacci(n - 1) + fibonacci(n - 2);
}
*/

var fibonacciCache = [0, 1];

var fibonacci = function(n) {
	if(n == 1) return fibonacciCache[0];
	if(n == 2) return fibonacciCache[1];
	
	if(fibonacciCache[n -1] == undefined) {
		fibonacciCache[n - 1] = fibonacci(n - 1) + fibonacci(n - 2);
	}
	
	return fibonacciCache[n - 1];
}

for(var i = 1; i <= 1477; i++) {
	console.log(i + "f = " + fibonacci(i));
}

