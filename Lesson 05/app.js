'use strict'
console.log("Programming Fundamentals for Beginners");

/* Արտածել a, b և c կամայական թվերը աճման կարգով։ */


/* Արտածել 1-ից մինչև n բոլոր ամբողջ թվերը։ */
var n = 16;
var i = 1;

while(i <= n) {
	console.log(i);
	//i = i + 1;
	i += 1;
}

/* Արտածել 1-ից մինչև n բոլոր ամբողջ զույգ թվերը։ */
n = 17;
i = 1;

/*
while(i <= n) {
	if(i % 2 == 1) {
		console.log(i);
	}
	//i = i + 1;
	i += 1;
}
*/

var a = 3;
var b = 111;

if(a % 2 != 0) a += 1;

while(a <= b) {
	console.log(a);
	a += 2;
}

/* Արտածել 1-ից մինչև n բոլոր ամբողջ կենտ թվերը։ */

/* Հաշվել 1-ից մինչև n բոլոր ամբողջ թվերի գումարը և արտածել այն։ */
var sum = 0;
var i = 1;
var n = 100;

while(i <= n) {
	sum += i; //sum = sum + i;
	i += 1;
}

console.log(sum);

/* Հաշվել 1-ից մինչև 15 բոլոր ամբողջ թվերի արտադրյալը և արտածել այն։ */
var p = 1;
var i = 1;
var n = 200;

while(i <= n) {
	p *= i;
	console.log(i + "! = " + p);
	i += 1;
}

//arrays
console.log("--------------");
var t = [12.3, 12, 11, 5, -2, 0, 8];
var i = 0;
var sum = 0;

while(i < t.length) {
	console.log(i + " " + t[i]);
	sum += t[i];
	i += 1;
}

console.log("sum = " + sum);
console.log("t avr = " + sum / t.length);