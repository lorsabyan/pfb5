'use strict';
console.log("Programming Fundamentals for Beginners");

var WriteHeading = function(text, size) {
	
	switch(size) {
		case 1: 
			document.writeln("<h1>" + text + "</h1>");
			break;
		case 2: 
			document.writeln("<h2>" + text + "</h2>");
			break;
		case 3: 
			document.writeln("<h3>" + text + "</h3>");
			break;
		case 4: 
			document.writeln("<h4>" + text + "</h4>");
			break;
		case 5: 
			document.writeln("<h5>" + text + "</h5>");
			break;
		case 6: 
			document.writeln("<h6>" + text + "</h6>");
			break;
	}
}

var WriteTag = function(text, tag) {
	var result = "<" + tag + ">" + text + "</" + tag + ">";
	document.writeln(result);
}

for (var i = 1; i <= 6; i++) {
	if(i == 3) continue;
	WriteHeading("Heading " + i, i);
}

WriteTag("This is a paragraph", "p");
WriteTag("This is a heading 1", "h1");
WriteTag("This is a strong &lt;text&gt;", "strong");

var OnClick = function() {
	var output = document.getElementById("output");
	var input = document.getElementById("input");
	output.innerHTML = "<p>" + input.value + "</p>" + output.innerHTML;
	input.value = "";
}

var Googling = function() {
	var term = document.getElementById("term").value;
	window.location = "http://google.com?q=" + term;
}