'use strict';
console.log("Programming Fundamentals for Beginners");

/* Գրել ֆունկցիա, որը կվերադարձնի արգումենտում փոխանցված թվային զանգվածը 
հակառակ կարգով։ */

var ReverseArray = function(array) {
	var swap;
	for (var i = 0; i < array.length / 2; i++) {
		swap = array[i];
		array[i] = array[array.length - i - 1];
		array[array.length - i - 1] = swap;
	}
}

var AddArray = function(a, b) {
	var c = [];
	for (var i = 0; i < a.length; i++) {
		c[i] = a[i] + b[i];
	}
	return c;
}

var MulArray = function(a, b) {
	var c = [];
	for (var i = 0; i < a.length; i++) {
		c[i] = a[i] * b[i];
	}
	return c;
}

var GetArrayPart = function(array, from, to) {
	var result = [];
	for (var i = from, j = 0; i < to; i++, j++) {
		result[j] = array[i];
	}
	return result;
}

var PrintAllMinutesInDay = function() {
	for (var h = 0; h < 24; h++) {
		for(var m = 0; m < 60; m++) {
			console.log((h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m));
		}
	}
}

PrintAllMinutesInDay();

var myArray = [1,2,3,4,5,6,7];
var subarray = GetArrayPart(myArray, 1, 4);
console.log(subarray);


var x = [1,2,3,4,5];
var y = [1,2,3,4,5,6];

PrintArray(x, "x");
PrintArray(y, "y");

ReverseArray(x);
ReverseArray(y);

PrintArray(x, "x");
PrintArray(y, "y");

var a = [1, 2, 3];
var b = [4, 5, 6];

console.log(AddArray(a, b));
console.log(MulArray(a, b));




var MoveByCommand = function(command) {
	switch(command) {
		case "Up":
			console.log("Move ↑");
			break;
		case "Down":
			console.log("Move ↓");
			break;
		case "Right":
			console.log("Move →");
			break;
		case "Left":
			console.log("Move <-");
			break;
		default:
			console.log("Unknown Command");
	}
}

MoveByCommand("Down");
MoveByCommand("Down");
MoveByCommand("Right");
MoveByCommand("Doun");