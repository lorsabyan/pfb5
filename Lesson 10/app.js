'use strict';
console.log("Programming Fundamentals for Beginners");

var i = 0;

do {
	console.log(++i);
}
while(i < 10)

var jaggedArray = [
	[1, 2, 3],
	[1, 2, 3, 4, 5],
	[1, 2]
];

var s;

for (var i = 0; i < jaggedArray.length; i++) {
	s = 0;
	for (var j = 0; j < jaggedArray[i].length; j++) {
		s += jaggedArray[i][j];
	}
	console.log(jaggedArray[i] + " sum = " + s);
}

//*******************************
console.log("----------------------");
var trash = [2, "hello", false, undefined, NaN, [1, 2], function() {}];
console.log(trash);
for (var i = 0; i < trash.length; i++) {
	switch(typeof(trash[i])) {
		case "boolean":
			console.log(!trash[i]);
			break;
		case "string":
			console.log(trash[i] + "!!!");
			break;
		case "undefined":
			console.log(trash[i]);
			break;
		case "number":
			console.log(++trash[i]);
			break;
		case "object":
			console.log(trash[i]);
			break;
		case "function":
			console.log(trash[i]);
			break;
	}
}

var RandomNumber = function(min, max) {
	var r = Math.random();
	return (max - min) * r + min;
}

var GeneratePassword = function(length) {
	var password = "";
	var next;
	for (var i = 0; i < length; i++) {
		next = Math.round(RandomNumber(65, 90));
		password += String.fromCharCode(next);
	}

	return password;
}


for (var i = 1; i <= 100; i++) {
	console.log(i + ": " + GeneratePassword(30));
}