'use strict';
console.log("Programming Fundamentals for Beginners");

var myObject = {};

myObject[0] = 0;
myObject[2] = 2;
myObject[1] = 1;

myObject["name"] = "John";

myObject.age = 20;

myObject.saySomething = function() {
	console.log("Hi");
	console.log("My name is " + this.name);
	console.log("I'm " + this.age);
	return this[0];
}

var properties = Object.getOwnPropertyNames(myObject);
console.log("------myObject properties--------")
for (var i = 0; i < properties.length; i++) {
	console.log(properties[i] + ": " + myObject[properties[i]]);
}
console.log("------myObject properties--------")
var b = 5;

var testFunction = function() {
	var a = "this is a";
	var b = "this is b";
};

testFunction();

var people = [
{
	"name": "John",
	"age": 25,
	"2": 0,
},
{
	name: "Bob",
	age: 35
}
];

/*
function myFunction() {

}
*/

var myFunction = function() {

};


// self-invoking function
(function () {
    
})();


// callback function
var greetings = function() {
	return "Hello ";
}

var sayHi = function() {
	return "Hi ";
}

var callbackTest = function(name, callback) {
	console.log(callback() + name);
}

callbackTest("John", greetings);
callbackTest("John", sayHi);