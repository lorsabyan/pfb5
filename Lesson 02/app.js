var message = "Hello";
var name = "John Smith";

console.log(message + " " + name);

var salary = 800000;
var salaryWithTaxes = salary + salary * 24.6 / 100;

console.log("Salary without taxes: " + salary);
console.log("Salary with taxes: " + salaryWithTaxes);