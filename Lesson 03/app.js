'use strict'

console.log("Programming Fundamentals for Beginners");

//Արտածեք Ձեր անունը, ազգանունը և տարիքը։
var name = "John Smith";
var age = 30;

console.log("Name: " + name);
console.log("Age: " + age);

/*
Հաշվել, թե a թիվը b թվի ո՞ր տոկոսն է կազմում 
օգտագործելով p = 100 * a / b բանաձևը։
*/
var a = 12.5;
var b = 87.03;
var p = (a / b) * 100;

console.log(a + " is " + p + "% of " + b);

//Հաշվել, թե քանի վայրկյան կա n քանակությամբ օրերում։
var n = 12;
var secondsInDay = 24 * 3600;
var seconds = secondsInDay * n; //24 * 60 * 60 * n

//Փոխակերպել դոլարը դրամի օգտագործելով օրվա փոխարժեքը։
var usd = 1000;
var rate = 480;
var amd = usd * rate;

/*
Հաշվել, թե քանի րոպեում կարելի է ներբեռնել 4.23 GB չափսի ֆայլը, 
եթե ինտերնետի միջին արագությունը 12.5 Mbit/s է։
*/
var sizeInGB = 4.23;
var speedInMbs = 12.5;

var speedInMBs = speedInMbs * 1000000 / 8 / 1024 / 1024;
var sizeInMB = sizeInGB * 1024;

var tSeconds = sizeInMB / speedInMBs;
var tMinutes = tSeconds / 60;

console.log(sizeInGB + "GB file download time is " + tMinutes + "min");

/*
Հաշվել, թե քանի MB կլինի 16 MP պարունակող նկարի չափսը, 
եթե ամեն pixel-ի համար անհրաժեշտ է 32 bit:
*/
var sizeInMPx = 16;
var sizeInPx = 16 * 1000000;
var bytesPerPixel = 32 / 8;
var sizeInBytes = sizeInPx * bytesPerPixel;
var sizeInMBs = sizeInBytes / (1024 * 1024);

console.log(sizeInMPx + "MP file size is " + sizeInMBs + "MB");