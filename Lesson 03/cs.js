 console.log("===== Conditional Statements =====");

//Գտնել երկու թվերից մեծագույնը։
var a = 55;
var b = 55;

var max;

if(a > b) {
	max = a;
}
else {
	max = b;
}

console.log("max of " + a  + " and " + b + " is " + max);


//alternative solution
max = a;

if(b > max) {
	max = b;
}